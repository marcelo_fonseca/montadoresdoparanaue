#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>		/*biblioteca para permitir o uso de booleanos*/
#include <ctype.h>			/*biblioteca pra passar as parada toda pra UPPERCASE*/

#define ROTULOVET 25												/*numero maximo de rotulos*/
#define MAXINTS 14													/*numero maximo de instrucoes*/
#define MAXDIRE	8 													/*Numero maximo de definicoes*/
#define TEXT 1														/*Identificador usado no int da struct instrucao e diretiva. Onde int 1 equivale a secao text*/
#define DATA 2														/*Identificador usado no int da struct instrucao e diretiva. Onde int 2 equivale a secao data*/
/*funcoes*/
	void	inicializarInstrucao();									/*no inicio do programa define as 14 instrucoes do assembly inventado*/
	void	inicializarRotulo();
	void	inicializarDiretivas();
	void	lexico(int linha);										/*Diz se eh erro lexico*/
	void	sintatico(int linha);									/*Diz se eh erro sintatico*/
	void	semantico(int linha);									/*Diz se eh erro semantico*/
	void	upperCase(char nome[]);
	void	idRotulo(char token[], int coluna);
	void	TabelaDeSimbolos(char token[], int coluna);
	void	TabelaDeDiretivas(char token[], int coluna, char rotulo[], int secao);
	void	TabelaDeInstrucao(char token[], int coluna, char rotulo[], int secao);
	void	montarTabelaDeSimbolos();
	void	preProc(char nomeArquivo[]);
	void	primeiraPassagem(char nomeArquivo[]);
	void	consultarOperacao(char label[], char operacao[], char op1[], char op2[], char offsetOp1[], char offsetOp2[]/*, int flag_text, int flag_data */);
	void	segundaPassagem(char nomeArquivo[]);
	void 	printSectionData(); 									/*imprime o cabecalho da secao data em IA-32*/
	void 	printSectionText();										/*imprime o cabecalho da secao text em IA-32*/
	void	sections(char token[], int *secao, bool *, bool *);						/*identificar e trata erros de secao*/
	void	verificaSecao(int secao, int posicao, bool inst);		/*verifica se a instrucao ou diretiva esta na secao errada*/
	void	secaoTextDataFaltante(bool data, bool text);			/*verifica se a secao text ou data esta ausente*/
	void 	ValidacaoRotulo(char token[]);							/*validacao do rotulo*/
	int     consultaRotulo(char op1[]);                             /*Consulta valor/posicao do rotulo*/
	void 	inicializar();											/*inicializacao do programa*/
	void 	validaLabel(char token[]);								/*verifica se existe mais de um rotulo na mesma linha*/
struct rotuloo{ /*live (idioma)*/
	bool declarado;
	bool publico;
	char tipo[10];					/*const, space, public, extern....*/
	char nome[20];
	unsigned short int posicao;
	int tamMemoria;
	int valor;
};
struct rotuloo rotulos[25];
struct instrucao{
	char nome[10];
	int opcode;
	int tamanho;
	int numOperandos;
	unsigned short int secao;			/* identificador: 1 secao text, 2 secao data, 0 nao declarado*/
};
struct instrucao instrucoes[16];
struct diretiva{
	char nome[10];
	unsigned short int tamanho;
	unsigned short int numOperandos;
	unsigned short int secao;			/*identificador 1 secao text, 2 secao data, 0 nao declarado*/
};
struct diretiva diretivas[8];
unsigned int contPosicao;		/*conta as posicoes de cada instrucao*/

int main(int argc, char *argv[]){
    char nomeArquivo[30];

    printf("argc:%d argv:%s\n", argc, argv[1]);  				/*pra testar se ta pegando o argumento certo. ta tranqs!*/
	if(argv[1] == NULL){
		printf("ERROR:entrada para Argv1 invalido. Leia o Readme.\a\a\n");
		exit(EXIT_FAILURE);
	}
    sprintf(nomeArquivo, argv[1]); 								/*no windows argv[0] ele pega o montador.exe, entao o segundo argumento, , eh o arquivo objeto. nomeArquivo recebe argv[1]*/
	inicializar();
	/*preProc(nomeArquivo);*/
	primeiraPassagem(nomeArquivo);								/*funcao onde sera feito a primeira passagem*/
	montarTabelaDeSimbolos();
    sprintf(nomeArquivo, argv[1]);
    segundaPassagem(nomeArquivo);/*<-----------------EQU da segmentation fault ta aqui em algum lugar*/
    return EXIT_SUCCESS;
}
void inicializar(){
	remove("arquivo.s"); 					/*remove o arquivo.s caso ja exista um*/
	inicializarInstrucao();					/*inicializa a struct que tem as 14 instrucoes*/
	inicializarRotulo();
	inicializarDiretivas();
	return;
}
void inicializarRotulo(){
	int i;
	for ( i = 0; i < ROTULOVET; ++i){
		sprintf(rotulos[i].nome, "empty");
		sprintf(rotulos[i].tipo, "empty");
		rotulos[i].posicao = 0;
		rotulos[i].tamMemoria = 0;
		rotulos[i].valor = 0;
	}
}
void inicializarInstrucao(){

	sprintf(instrucoes[1].nome, "ADD");
	instrucoes[1].opcode = 01;
	instrucoes[1].tamanho = 9;
	instrucoes[1].secao = TEXT;

	sprintf(instrucoes[2].nome, "SUB");
	instrucoes[2].opcode = 02;
	instrucoes[2].tamanho = 9;
	instrucoes[2].secao = TEXT;

	sprintf(instrucoes[3].nome, "MULT");
	instrucoes[3].opcode = 03;
	instrucoes[3].tamanho = 9;
	instrucoes[3].secao = TEXT;

	sprintf(instrucoes[4].nome, "DIV");
	instrucoes[4].opcode = 04;
	instrucoes[4].tamanho = 10;
	instrucoes[4].secao = TEXT;

	sprintf(instrucoes[5].nome, "JMP");
	instrucoes[5].opcode = 05;
	instrucoes[5].tamanho = 2;
	instrucoes[5].secao = TEXT;

	sprintf(instrucoes[6].nome, "JMPN");
	instrucoes[6].opcode = 06;
	instrucoes[6].tamanho = 5;
	instrucoes[6].secao = TEXT;

	sprintf(instrucoes[7].nome, "JMPP");
	instrucoes[7].opcode = 07;
	instrucoes[7].tamanho = 5;
	instrucoes[7].secao = TEXT;

	sprintf(instrucoes[8].nome, "JMPZ");
	instrucoes[8].opcode = 8;
	instrucoes[8].tamanho = 5;
	instrucoes[8].secao = TEXT;

	sprintf(instrucoes[9].nome, "COPY");
	instrucoes[9].opcode = 9;
	instrucoes[9].tamanho = 14;
	instrucoes[9].secao = TEXT;

	sprintf(instrucoes[10].nome, "LOAD");
	instrucoes[10].opcode = 10;
	instrucoes[10].tamanho = 7;
	instrucoes[10].secao = TEXT;

	sprintf(instrucoes[11].nome, "STORE");
	instrucoes[11].opcode = 11;
	instrucoes[11].tamanho = 7;
	instrucoes[11].secao = TEXT;

	sprintf(instrucoes[12].nome, "INPUT");
	instrucoes[12].opcode = 12;
	instrucoes[12].tamanho = 16;
	instrucoes[12].secao = TEXT;

	sprintf(instrucoes[13].nome, "OUTPUT");
	instrucoes[13].opcode = 13;
	instrucoes[13].tamanho = 16;
	instrucoes[13].secao = TEXT;

	sprintf(instrucoes[14].nome, "STOP");
	instrucoes[14].opcode = 14;
	instrucoes[14].tamanho = 12;
	instrucoes[14].secao = TEXT;
}
void inicializarDiretivas(){

	sprintf(diretivas[0].nome, "SECTION");
	diretivas[0].tamanho =0;
	diretivas[0].numOperandos=1;
	diretivas[0].secao = 0;

	sprintf(diretivas[1].nome, "SPACE");
	diretivas[1].tamanho =0;
	diretivas[1].numOperandos=1;
	diretivas[1].secao= DATA;

	sprintf(diretivas[2].nome, "CONST");
	diretivas[2].tamanho =1;
	diretivas[2].numOperandos=1;
	diretivas[2].secao= DATA;

	sprintf(diretivas[3].nome, "EQU");
	diretivas[3].tamanho =1;
	diretivas[3].numOperandos=1;
	diretivas[3].secao= 0;

	sprintf(diretivas[4].nome, "IF");
	diretivas[4].tamanho =0;
	diretivas[4].numOperandos=1;
	diretivas[4].secao= 0;
}
void lexico(int linha){
	printf("\nERRO LEXICO: linha %d.", linha);
	return;
}
void sintatico(int linha){
	printf("\nERRO SINTATICO: linha %d.", linha);
	return;
}
void semantico(int linha){
	printf("\nERRO SEMANTICO: linha %d.", linha);
	return;
}
void upperCase(char nome[]){ 															/*passa o nome para UPPERCASE*/
	short int i=0;
   	while(nome[i]){
      	nome[i]=toupper(nome[i]);
      	i++;
   	}
}
void primeiraPassagem(char nomeArquivo[]){									/*Analisa linha por linha. apos pegar a linha separar em r�tulo, opera��o, operandos e coment�rios*/
	int coluna=0, secao=0;							/*Coluna:conta as posicoes totais de colunas(numero de linhas); secao: verifica a secao atual dinamicamente. 0 nada, 1(TEXT) secao text, 2(DATA) secao data*/
	char linha[100];														/*segundo a ementa a linha deve ser 100*/
	char *token;
	int flag_input = 0, flag_output = 0, flag_space;
	char *rotulo = malloc(sizeof(*token));
	FILE *fp;
	bool text=false, data=false;

	linha[0]= '\0';
	contPosicao=0;
	strcat(nomeArquivo, ".asm");
	fp = fopen(nomeArquivo, "r");
	if(fp== NULL){															/*protecao anti segmentation fault*/
		printf("opa, deu ruim! o ponteiro 'fp' eh nulo");
		exit(0);
	}
	while(fgets(linha, sizeof(linha), fp)){									/*fgets pega a linha inteira, a coluna eh o contador do numero de linhas*/
		coluna++;															/*comeca no 1*/
		/*printf("\nlinha: %d |\t", coluna);*/									/*printa a linha atual em que se encontra a varredura*/
		flag_space = 0;
		token = strtok(linha, "\n");										/*passo a linha para o token. a quebra vai ser trabalhada em cima por isso a passagem agora.*/
		/*printf("token(linha):'%s' \n", token);*/
		if (token ==NULL)
			continue;
		if( memchr(linha, ';', strlen(linha))!= NULL){
			if(linha[0]== ';')
				continue;
			token = strtok(linha, ";");										/*elimina os comentarios*/
			/*token = strtok(token, " \t");
			if (token == NULL)
                continue;
            token = strtok(linha, ";");
			/*printf("\nachou ';'! retirando\n");*/
			/*puts(token);*/
		}
		if(memchr(token, ':', strlen(token))!= NULL){ /*Adicionar 2 tratamentos de erro. rotulo invalido e 2 rotulos na linha*//*PROCURA no token se existe o char ':'*/
			token = strtok(token, " \t:");										/*strtok vai separar a linha em tokens quando encontrar os caracteres listados*/
			/*printf("Label:'%s'\t", token);*/
			TabelaDeSimbolos(token, coluna); 									/*procura na tabela de simbolos, se achou, Erro: simbolo redefinido. senao define e insere posicao idRotulo(token);*/
			sprintf(rotulo,token);												/*rotulo encontrado em "token" eh passado para "rotulo"*/
			token = strtok(NULL, " \t");										/*pega operando*/
			validaLabel(token);
			TabelaDeInstrucao(token, coluna, rotulo, secao);
		} else {
			token = strtok(token, " \t\n");
			if(strcmp(token,"SECTION")==0){
				sections(token, &secao, &data, &text);							/*fucao para manipular sections data e text*/
				continue;
			} else{
				sprintf(rotulo,"vazio");
				upperCase(token);
				if(strcmp(token,"INPUT") == 0)
                    flag_input = 1;
                if(strcmp(token,"OUTPUT") == 0){
                    puts(token);
                    flag_output = 1;}
				TabelaDeInstrucao(token, coluna, rotulo, secao);				/*se nao achar na tabela de instrucao. procura na tabela de diretivas. se nao achar da erro*/
                if(strcmp(token,"STOP") == 0){
                    if (flag_input == 1){
                        puts("\nINPUT\n");
                        //strcpy(token,"LerInteiro");
                        idRotulo("LerInteiro\0",coluna);
                        printf("%d",coluna);
                        contPosicao += 31;
                    }
                    if (flag_output == 1){
                        /*puts("\nOUTPUT\n");*/
                        //strcpy(token,"LerInteiro");
                        idRotulo("EscreverInteiro\0",coluna);
                        contPosicao += 31;
                    }
                    if ((flag_output == 1)||(flag_input == 1)){
                        idRotulo("buffer\0",coluna);
                        contPosicao += 1;
                    }
                }
			}
		}
		token = strtok(NULL, "\0\n");
		if (token == NULL){														/*CAI AQUI QUANDO EH STOP*/
			puts(token);
			/*printf("Nenhum operando encontrado!!\n");*/
			/*if(strcmp(token,"STOP") == 0){
                puts("\naaaa\n");
                if (flag_input == 1){
                puts("\nINPUT\n");
                //strcpy(token,"LerInteiro");
                idRotulo("LerInteiro\0",coluna);
                }
                contPosicao += 30;
                if (flag_output == 1){
                puts("\nOUTPUT\n");
                //strcpy(token,"LerInteiro");
                idRotulo("EscreverInteiro\0",coluna);
                }
                contPosicao += 30;
            ]*/
			continue;
		}
		if(memchr(token, ',', strlen(token))!= NULL){ 							/*PROBLEMA: lendo a instrucao e nao acha a ","*/
			token = strtok(token, ","); 										/*ler ateh a virgula e quebrar o operando certinho*/
			/*printf("Primeiro Operando:%s \n", token); */
			token = strtok(NULL , " \t\n");
			/*printf("Segundo Operando:%s\n", token); */
		} else {
			/*printf("Unico Operando:%s\n", token);*/
			token = strtok(NULL , " \t\n");
		}
		/*************************************/
		while( token != NULL)													/*continuara lendo ate chegar no final da linha */
			token = strtok(NULL, " \n\t");
		/*************************************/
	}
	secaoTextDataFaltante(data,text);											/*verifica se tem secao text ou data faltante*/
	rewind(fp);
	fclose(fp);
}
void segundaPassagem(char nomeArquivo[]){
    contPosicao = 0;
    int i=0;
    int coluna=0;
	char linha[100];														/*segundo a ementa a linha deve ser 100*/
	char *token;
	char *rotulo = malloc(sizeof(*token));
	char label[20], operacao[10], op1[10], op2[10], offsetOp1[10], offsetOp2[10];
	/*int flag_text = 0, flag_data = 0;*/
	FILE *fp;
	linha[0]= '\0';
	contPosicao=0;
	strcat(nomeArquivo, ".asm");
	fp = fopen(nomeArquivo, "r");
	while(fgets(linha, sizeof(linha), fp)){									/*fgets pega a linha inteira, a coluna eh o contador do numero de linhas*/
		label[0] = '\0';
		operacao[0] = '\0';
		op1[0] = '\0';
		op2[0] = '\0';
		offsetOp1[0] = '\0';
		offsetOp2[0] = '\0';
		coluna++;															/*comeca no 1*/
		/*printf("\nlinha: %d |\t", coluna);*/									/*printa a linha atual em que se encontra a varredura*/
		token = strtok(linha, "\n");										/*passo a linha para o token. a quebra vai ser trabalhada em cima por isso a passagem agora.*/
		/*printf("token(linha):'%s' \n", token);*/
		if (token == NULL)
			continue;
		if( memchr(linha, ';', strlen(linha))!= NULL){
			if(linha[0]== ';')
				continue;
			token = strtok(linha, ";");										/*elimina os comentarios*/
			/*token = strtok(token, " \t");
			if (token == NULL)
                continue;
            token = strtok(linha, ";");
			/*printf("\nachou ';'! retirando\n");*/
			/*printf("\n%s",token);*/
		}
		if(memchr(token, ':', strlen(token))!= NULL){							/*PROCURA no token se existe o char ':'*/
			token = strtok(token, " \t:");										/*strtok vai separar a linha em tokens quando encontrar os caracteres listados*/
			strcpy(label,token);
			/*printf("\n%s",label);*/
			/*printf("Label:'%s'\t", token);*/
			/*TabelaDeSimbolos(token, coluna); 	*/								/*procura na tabela de simbolos, se achou, Erro: simbolo redefinido. senao define e insere posicao idRotulo(token);*/
			/*sprintf(rotulo,token);*/
			token = strtok(NULL, " \t");										/*pega operando*/
			strcpy(operacao, token);
		} else {
			token = strtok(token, " \t\n");
			upperCase(token);
			if(strcmp(token,"SECTION")==0){
				token= strtok(NULL, " \t\n");
				if(token == NULL)
					continue;
                upperCase(token);
				if (strcmp(token,"TEXT")==0){
                    /*printf("\n\nsection .text");
                    printf("\nglobal _start");*/
                    /*flag_text = 1;
                    flag_data = 0;*/
                    printSectionText();

				} else if(strcmp(token,"DATA")==0){
				    /*printf("\n\nsection .data");*/
				    /*flag_data = 1;
                    flag_text = 0;*/
					printSectionData();
				}
				continue;
			} else{
			    strcpy(operacao,token);
                /*consultarOperacao(token);
				//sprintf(rotulo,"vazio");
				//TabelaDeInstrucao(token, coluna, rotulo);	*/						/*se nao achar na tabela de instrucao. procura na tabela de diretivas. se nao achar da erro*/
			}
		}
		token = strtok(NULL, "\0\n");
		if (token == NULL){														/*CAI AQUI QUANDO EH STOP*/
			/*printf("Nenhum operando encontrado!!\n");
			//op1[0] = '\0';
			//op2[0] = '\0';*/
			consultarOperacao(label,operacao,op1,op2,offsetOp1,offsetOp2/*,flag_text,flag_data*/);
			continue;
		}
		if(memchr(token, ',', strlen(token))!= NULL){ 							/*PROBLEMA: lendo a instrucao e nao acha a ","*/
			token = strtok(token, ","); 										/*ler ateh a virgula e quebrar o operando certinho*/
            strcpy(op1,token);
			/*printf("Primeiro Operando:%s \n", token);*/
			token = strtok(NULL , " \t\n");
			strcpy(op2,token);
			/*printf("%s\t%s\t%s\t%s\t",label,operacao,op1,op2);
			//printf("Segundo Operando:%s\n", token);*/
			consultarOperacao(label,operacao,op1,op2,offsetOp1,offsetOp2/*,flag_text,flag_data*/);
		} else {
			/*printf("Unico Operando:%s\n", token);
			//consultarOperacao(operacao);*/
			token = strtok(token , " \t\n");
			if(memchr(token, '+', strlen(token))!= NULL){   /*procura o char '+'. Se achar*/
                token = strtok(token, "+");
                /*token = strtok(token, " \t");*/
                strcpy(op1,token);
                /*op1 = strtok(op1," \t");*/
                puts(op1);
                token = strtok(NULL , " \t\n");
                strcpy(offsetOp1,token);
                puts(token);
                /*pra validar o acesso: Comparar o tamanho do rotulo com o offset.
                * Se offset > tamanho																	*/
			}
			else
                strcpy(op1,token);
			/*puts(op1);
			//op2[0] = '\0';*/
			if (strcmp(operacao, "IF") == 0){
                i = consultaRotulo(op1);
                while( token != NULL)													/*continuara lendo ate chegar no final da linha */
                    token = strtok(NULL, " \n\t");
                if (rotulos[i].valor != 1)
                    fgets(linha, sizeof(linha), fp);
                continue;
            }
			consultarOperacao(label,operacao,op1,op2,offsetOp1,offsetOp2/*,flag_text,flag_data*/);
			/*op2[0] = '\0';
			//printf("%s\t%s\t%s\t%s\t",label,operacao,op1,op2);
			//consultarOperacao(label,operacao,op1,op2);
			//puts(token);*/
		}
		while( token != NULL)													/*continuara lendo ate chegar no final da linha */
			token = strtok(NULL, " \n\t");
	}
	rewind(fp);
	fclose(fp);
	free(rotulo);
}
void idRotulo(char token[], int coluna){ 													/*chamada na primeira passagem*/
	short int i=0;
  	/*upperCase(token);*/												/*chama funca para deixar em uppercase*/
	while ((strcmp(rotulos[i].nome, "empty") !=0)){
		i++;
		if (!(i == ROTULOVET)){
			continue;
		} else{
			printf("ERROR:Vetor de rotulos esta cheio!\n");
			exit(EXIT_FAILURE);
		}
	}
	sprintf(rotulos[i].nome, token);
	rotulos[i].declarado = true;
	/*puts(diretivas[i].nome);*/
	/*if (strcmp(diretivas[i].nome, "EQU"))
        rotulos[i].posicao = -1;
    else*/
    rotulos[i].posicao = contPosicao;
	printf("\nLabel '%s' gravado em rotulos[%d] Posicao:%d \n", token, i, contPosicao);
}
void TabelaDeSimbolos(char token[], int coluna){ 											/*chamada na primeira passagem*/
	int i=0;
	ValidacaoRotulo(token);
	while ((strcmp(rotulos[i].nome, token) !=0)){
		i++;
		if (!(i == ROTULOVET)){
			continue;
		} else{
			printf("Rotulo nao encontrado. Declarando\n");
			idRotulo(token, coluna);
			return;
		}
	}
	semantico(coluna);
	printf("ERROR: simbolo redefinido. \n");
	exit(0);
}
void TabelaDeDiretivas(char token[], int coluna, char rotulo[], int secao){							/*Chamada na primeira passagem */
	short int i=0, j=0;
	upperCase(token);
	while (strcmp(diretivas[i].nome, token)!=0){								/*percorre todo o vetor struct de diretivas*/
		i++;
		if(!(i>=MAXDIRE)){														/*se o valor de i for menor que o valor total de diretivas. continue*/
			continue;
		} else {
			sintatico(coluna);
			printf("ERROR: Diretiva nao encontrado. Diretiva ou instrucao invalida. \n");
			exit(0);
		}
	}
	printf("Diretiva:'%s' encontrada \n", diretivas[i].nome);
	verificaSecao(secao,i,false);												/*Verifica se esta na secao certa.terceiro parametro eh um booleano para diferenciar diretiva de instrucao*/
	contPosicao += diretivas[i].tamanho; 										/*contador_posi��o = valor retornado pela subrotina*/
	if (strcmp(rotulo, "vazio")==0){
		return;																	/*verifica se existe um rotulo na linha. senao retorna*/
	} else{
		while(strcmp(rotulo,rotulos[j].nome)!=0 ){
			j++;
			if(!(j>=ROTULOVET)){
				continue;
			} else{
				semantico(coluna);
				printf("ERROR: rotulo '%s' nao foi encontrado\n");
				exit(EXIT_FAILURE);
				return;
			}
		}
		token=strtok(NULL, " \t\n"); 													/*percorre para encontrar valor de diretivas spaces e consts*/
		if(token !=NULL){																/*se nao for nulo deve ser um ex: CONST 3 OU SPACE 3. Entra no if para tratar cada caso*/
			if(strcmp(diretivas[i].nome, "CONST")==0){									/*se for const, representa o valor da constante*/
				printf("valor da diretiva '%s'. sera?:%s\n", token, diretivas[i].nome);
				if(memchr(token, 'x', strlen(token))!= NULL)							/*verifica se eh hexadecimal*/
					rotulos[j].valor = (int)strtol(token,NULL,16);
				else
					rotulos[j].valor = atoi(token); 									/* atoi passa de string pra inteiro. ARRUMAR CASTING*/
				printf("rotulos[j].valor:%d\n", rotulos[j].valor);
				return;
			}
			else if(strcmp(diretivas[i].nome, "SPACE")==0){									/*se for space o numero representa tamanho da memoria*/
				printf("tamanho da diretiva %s. sera?:%s\n", token, diretivas[i].nome);
				rotulos[j].tamMemoria = atoi(token); 									/* atoi passa de string pra inteiro. ARRUMAR CASTING*/
				contPosicao += rotulos[j].tamMemoria;
				printf("rotulos[j].tamMemoria:%d\n", rotulos[j].tamMemoria);
				return;
			}
			else if(strcmp(diretivas[i].nome, "EQU")==0){
                if(memchr(token, 'x', strlen(token))!= NULL)							/*verifica se eh hexadecimal*/
					rotulos[j].valor = (int)strtol(token,NULL,16);
				else
					rotulos[j].valor = atoi(token); 									/* atoi passa de string pra inteiro. ARRUMAR CASTING*/
				printf("rotulos[j].valor:%d\n", rotulos[j].valor);
				return;
			}
		}
		rotulos[j].tamMemoria = diretivas[i].tamanho;									/*poem o tamanho de memoria reservado da diretiva*/
		sprintf(rotulos[j].tipo,diretivas[i].nome);										/*no rotulo fica armazenado o tipo dela. SPACE ou CONST*/
		printf("%s com tamanho %d DEU CERTO\n", rotulo,rotulos[j].tamMemoria);			/*#teste. #verificacao. #workhardPlayhard.*/
	}
}
void TabelaDeInstrucao(char token[], int coluna, char rotulo[], int secao){  							/*Chamada na primeira passagem*/
	short int i=0;
	upperCase(token);
	while (strcmp(instrucoes[i].nome, token)!=0){
		i++;
		if(!(i>MAXINTS)){
			continue;
		} else {
			printf("instrucao nao encontrado. Procurando na tabela de diretivas.\n");
			TabelaDeDiretivas(token, coluna, rotulo, secao);
			return;
		}
	}
	printf("Instrucao: %s encontrado \n", instrucoes[i].nome);
	verificaSecao(secao,i,true); 															/*terceiro parametro eh um booleano para diferenciar instrucao de diretiva*/
	contPosicao += instrucoes[i].tamanho;
}
void montarTabelaDeSimbolos(){																/*chamada na primeira passagem*/
	FILE *fp;
	short int i=0;
	fp=fopen("TabelaDeSimbolos.txt","w");
	if(fp==NULL){
		printf("meep\n");
		exit(0);
	}
	fprintf(fp, "TABELA DE SIMBOLOS\n");
	for (i = 0; i < ROTULOVET; ++i){

		if(strcmp(rotulos[i].nome ,"empty")==0)
			break;
        printf("%s\t %d\t%d\n",rotulos[i].nome,rotulos[i].posicao,rotulos[i].valor);
		fprintf(fp, "%s\t %d\n",rotulos[i].nome,rotulos[i].posicao);
	}
	rewind(fp);
	fclose(fp);
}
void preProc(char nomeArquivo[]){
    int coluna=0;
	char linha[100];														/*segundo a ementa a linha deve ser 100*/
	char *token;
	char *rotulo = malloc(sizeof(*token));
	FILE *fp;
	/*bool text=false, data=false;*/
	/*int space_equ[25][2];*/

	linha[0]= '\0';
	contPosicao=0;
	strcat(nomeArquivo, ".asm");
	fp = fopen(nomeArquivo, "r");
	if(fp== NULL){															/*protecao anti segmentation fault*/
		printf("opa, deu ruim! o ponteiro 'fp' eh nulo");
		exit(0);
	}
	while(fgets(linha, sizeof(linha), fp)){									/*fgets pega a linha inteira, a coluna eh o contador do numero de linhas*/
	coluna++;															/*comeca no 1*/
		/*printf("\nlinha: %d |\t", coluna);	*/								/*printa a linha atual em que se encontra a varredura*/
		token = strtok(linha, "\n");										/*passo a linha para o token. a quebra vai ser trabalhada em cima por isso a passagem agora.*/
		/*printf("token(linha):'%s' \n", token);*/

		if (token ==NULL)
			continue;
		if( memchr(linha, ';', strlen(linha))!= NULL){
			if(linha[0]== ';')
				continue;
			token = strtok(linha, ";");										/*elimina os comentarios*/
			/*printf("\nachou ';'! retirando\n");*/
			puts(token);
		}
		if(memchr(token, ':', strlen(token))!= NULL){							/*PROCURA no token se existe o char ':'*/
			token = strtok(token, " \t:");										/*strtok vai separar a linha em tokens quando encontrar os caracteres listados*/
			/*printf("Label:'%s'\t", token); */
			TabelaDeSimbolos(token, coluna); 									/*procura na tabela de simbolos, se achou, Erro: simbolo redefinido. senao define e insere posicao idRotulo(token);*/
			sprintf(rotulo,token);
			token = strtok(NULL, " \t");										/*pega operando*/
			TabelaDeInstrucao(token, coluna, rotulo, 0);
		}
	}
}
int consultaRotulo(char op1[]){
    int i = 0;
    while ((strcmp(rotulos[i].nome, op1) != 0)){
        i++;
        if (!(i == ROTULOVET)){
            continue;
        } else{
            printf("ERRO! Rotulo nao encontrado. \n");
            /*idRotulo(token, coluna);*/
            break;
        }
    }
    return i;
}
/*void consultarOperacao(char label[], char operacao[], char op1[], char op2[], int flag_text, int flag_data){*/
void consultarOperacao(char label[], char operacao[], char op1[], char op2[], char offsetOp1[], char offsetOp2[]/*, int flag_text, int flag_data*/){

	FILE *fp;
	int i, desloca;
	/*int le_int, escreve_int;*/
	char hex[33], hexaux[5];
	fp=fopen("arquivo.s", "a");
	if(fp==NULL){
		printf("holy guacamole!!\n");
		exit(EXIT_FAILURE);
	}

    upperCase(operacao);
    printf("%d\n",contPosicao);
    if (strcmp(operacao, "EQU") == 0){
        printf("%s\tEQU\t%s\n",label,op1);
        fprintf(fp,"%s\tEQU\t%s\n",label,op1);
        contPosicao += 1;
    }
    else if (strcmp(operacao, "CONST") == 0){
        printf("\t%s\tEQU\t%s\n",label,op1);
        fprintf(fp,"\t%s\tEQU\t%s\n",label,op1);
        contPosicao += 1;
    }
    else if (strcmp(operacao, "SPACE") == 0){
        if ((op1[0] == '\0')||(strcmp(op1, "1") == 0)){
            printf("\t%s\tdd\t0\n",label);
            fprintf(fp,"\t%s\tdd\t0\n",label);
            /*contPosicao += 1;*/
        }
        else{
            printf("\t%s\ttimes %s dd 0\n",label,op1);
            fprintf(fp,"\t%s\ttimes %s dd 0\n",label,op1);
            contPosicao += atoi(op1);
        }

    }
    else if (strcmp(operacao, "ADD") == 0){
        if(label[0] != '\0'){
            strcat(label,":");
        }
        i = consultaRotulo(op1);
        /*printf("%d", rotulos[i].valor);*/
        sprintf(hexaux,"%.2x", rotulos[i].valor);
        /*printf("%d", rotulos[i].valor);*/
        strcpy(hex,"8b 1c 25 ");
        strcat(hex, hexaux);
        strcat(hex," 00 00 00 ");
        /*puts("\n");
        puts(hex);
        puts("\n");*/
        if(offsetOp1[0] == '\0'){
            printf("%s\tmov\tebx, [%s]\t\t%s\n",label,op1,hex);
            printf("\tadd\teax, ebx\t\t01 d8 \n");
            fprintf(fp,"%s\tmov\tebx, [%s]\t\t%s\n",label,op1,hex);
            fprintf(fp,"\tadd\teax, ebx\t\t01 d8 \n");
        }
        else{
            printf("%s\tmov\tebx, [%s + 4 * %s]\t\t%s\n",label,op1,offsetOp1,hex);
            printf("\tadd\teax, ebx\t\t01 d8 \n");
            fprintf(fp,"%s\tmov\tebx, [%s + 4 * %s]\t\t%s\n",label,op1,offsetOp1,hex);
            fprintf(fp,"\tadd\teax, ebx\t\t01 d8 \n");
        }
        contPosicao += 9;
    }
    else if (strcmp(operacao, "SUB") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        i = consultaRotulo(op1);
        /*printf("%d", rotulos[i].valor);*/
        sprintf(hexaux,"%.2x", rotulos[i].valor);
        /*printf("%d", rotulos[i].valor);*/
        strcpy(hex,"8b 1c 25 ");
        strcat(hex, hexaux);
        strcat(hex," 00 00 00 ");

        if(offsetOp1[0] == '\0'){
            printf("%s\tmov\tebx, [%s]\t\t%s\n",label,op1,hex);
            printf("\tsub\teax, ebx\t\t29 d8 \n");
            fprintf(fp,"%s\tmov\tebx, [%s]\t\t%s\n",label,op1,hex);
            fprintf(fp,"\tsub\teax, ebx\t\t29 d8 \n");
        }
        else{
            printf("%s\tmov\tebx, [%s + 4 * %s]\t\t%s\n",label,op1,offsetOp1,hex);
            printf("\tsub\teax, ebx\t\t29 d8 \n");
            fprintf(fp,"%s\tmov\tebx, [%s + 4 * %s]\t\t%s\n",label,op1,offsetOp1,hex);
            fprintf(fp,"\tsub\teax, ebx\t\t29 d8 \n");
        }
        contPosicao += 9;
    }
    else if (strcmp(operacao, "MULT") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        i = consultaRotulo(op1);
        /*printf("%d", rotulos[i].valor);*/
        sprintf(hexaux,"%.2x", rotulos[i].valor);
        /*printf("%d", rotulos[i].valor);*/
        strcpy(hex,"8b 1c 25 ");
        strcat(hex, hexaux);
        strcat(hex," 00 00 00 ");

        if(offsetOp1[0] == '\0'){
            printf("%s\tmov\tebx, [%s]\t\t%s\n",label,op1,hex);
            printf("\tmul\tebx\t\tf7 e3 \n");
            fprintf(fp,"%s\tmov\tebx, [%s]\t\t%s\n",label,op1,hex);
            fprintf(fp,"\tmul\tebx\t\tf7 e3 \n");
        }
        else{
            printf("%s\tmov\tebx, [%s + 4 * %s]\t\t%s\n",label,op1,offsetOp1,hex);
            printf("\tmul\tebx\t\tf7 e3 \n");
            fprintf(fp,"%s\tmov\tebx, [%s + 4 * %s]\t\t%s\n",label,op1,offsetOp1,hex);
            fprintf(fp,"\tmul\tebx\t\tf7 e3 \n");
        }
        contPosicao += 9;
    }
    else if (strcmp(operacao, "DIV") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        i = consultaRotulo(op1);
        /*printf("%d", rotulos[i].valor);*/
        sprintf(hexaux,"%.2x", rotulos[i].valor);
        /*printf("%d", rotulos[i].valor);*/
        strcpy(hex,"8b 1c 25 ");
        strcat(hex, hexaux);
        strcat(hex," 00 00 00 ");

        if(offsetOp1[0] == '\0'){
            printf("%s\tmov\tebx, [%s]\t\t%s\n",label,op1,hex);
            printf("\tcdq\t\t99\n");
            printf("\tdiv\tebx\t\tf7 f3 \n");
            fprintf(fp,"%s\tmov\tebx, [%s]\t\t%s\n",label,op1,hex);
            fprintf(fp,"\tcdq\t\t99\n");
            fprintf(fp,"\tdiv\tebx\t\tf7 f3 \n");
        }
        else{
            printf("%s\tmov\tebx, [%s + 4 * %s]\t\t%s\n",label,op1,offsetOp1,hex);
            printf("\tcdq\t\t99\n");
            printf("\tdiv\tebx\t\tf7 f3 \n");
            fprintf(fp,"%s\tmov\tebx, [%s + 4 * %s]\t\t%s\n",label,op1,offsetOp1,hex);
            fprintf(fp,"\tcdq\t\t99\n");
            fprintf(fp,"\tdiv\tebx\t\tf7 f3 \n");
        }
        contPosicao += 10;
    }
    else if (strcmp(operacao, "JMP") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        contPosicao += 2;
        i = consultaRotulo(op1);
        desloca = contPosicao - rotulos[i].posicao;
        /*printf("%d", desloca);*/
        desloca = -2 - desloca;
        /*printf("%d", desloca);*/
        sprintf(hexaux,"%.4x",desloca);
        /*puts(hexaux);*/
        strcat(hex,"eb ");
        strncat(hex, hexaux, 2);
        strncat(hex, hexaux, 2);
        /*puts(hex);*/
        /*sprintf(hexaux,"%.2x", rotulos[i].posicao);
        printf("%d", rotulos[i].valor);*/
        printf("%s\tjmp\t%s\t\teb 00 \n",label,op1);
        fprintf(fp,"%s\tjmp\t%s\t\teb 00 \n",label,op1);
    }
    else if (strcmp(operacao, "JMPN") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        contPosicao += 5;
        i = consultaRotulo(op1);
        desloca = contPosicao - rotulos[i].posicao;
        /*printf("%d", desloca);*/
        desloca = -2 - desloca;
        /*printf("%d", desloca);*/
        sprintf(hexaux,"%.4x",desloca);
        /*puts(hexaux);*/

        printf("%s\tcmp eax, 0\t\t83 f8 00 \n",label);
        printf("\tjb\t%s\t\t72 00 \n",op1);
        fprintf(fp,"%s\tcmp eax, 0\t\t83 f8 00 \n",label);
        fprintf(fp,"\tjb\t%s\t\t72 00 \n",op1);
    }
    else if (strcmp(operacao, "JMPP") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        contPosicao += 5;
        i = consultaRotulo(op1);
        desloca = contPosicao - rotulos[i].posicao;
        /*printf("%d", desloca);*/
        desloca = -2 - desloca;
        /*printf("%d", desloca);*/
        sprintf(hexaux,"%.4x",desloca);
        /*puts(hexaux);*/

        printf("%s\tcmp eax, 0\t\t83 f8 00 \n",label);
        printf("\tja\t%s\t\t77 00 \n",op1);
        fprintf(fp,"%s\tcmp eax, 0\t\t83 f8 00 \n",label);
        fprintf(fp,"\tja\t%s\t\t77 00 \n",op1);
    }
    else if (strcmp(operacao, "JMPZ") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        contPosicao += 5;
        i = consultaRotulo(op1);
        desloca = contPosicao - rotulos[i].posicao;
        /*printf("%d", desloca);*/
        desloca = -2 - desloca;
        /*printf("%d", desloca);*/
        sprintf(hexaux,"%.4x",desloca);
        /*puts(hexaux);*/

        printf("%s\tcmp eax, 0\t\t83 f8 00 \n",label);
        printf("\tje\t%s\t\t74 00 \n",op1);
        fprintf(fp,"%s\tcmp eax, 0\t\t83 f8 00 \n",label);
        fprintf(fp,"\tje\t%s\t\t74 00 \n",op1);
    }
    else if (strcmp(operacao, "COPY") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        i = consultaRotulo(op1);
        /*printf("%d", rotulos[i].valor);*/
        sprintf(hexaux,"%.2x", rotulos[i].valor);
        /*printf("%d", rotulos[i].valor);*/
        strcpy(hex,"8b 1c 25 ");
        strcat(hex, hexaux);
        strcat(hex," 00 00 00 ");

        printf("%s\tmov\tebx, [%s]\t\t%s\n",label,op1,hex);
        fprintf(fp,"%s\tmov\tebx, [%s]\t\t%s\n",label,op1,hex);

        i = consultaRotulo(op2);
        /*printf("%d", rotulos[i].valor);*/
        sprintf(hexaux,"%.2x", rotulos[i].valor);
        /*printf("%d", rotulos[i].valor);*/
        strcpy(hex,"89 1c 25 ");
        strcat(hex, hexaux);
        strcat(hex," 00 00 00 ");

        printf("\tmov\tdword [%s], ebx\t\t%s \n",op2,hex);
        fprintf(fp,"\tmov\tdword [%s], ebx\t\t%s \n",op2,hex);
        contPosicao += 14;
    }
    else if (strcmp(operacao, "LOAD") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        i = consultaRotulo(op1);
        /*printf("%d", rotulos[i].valor);*/
        sprintf(hexaux,"%.2x", rotulos[i].valor);
        /*printf("%d", rotulos[i].valor);*/
        strcpy(hex,"8b 04 25 ");
        strcat(hex, hexaux);
        strcat(hex," 00 00 00 ");

        if (offsetOp1[0] == '\0'){
            printf("%s\tmov\teax, [%s]\t\t%s\n",label,op1,hex);
            fprintf(fp,"%s\tmov\teax, [%s]\t\t%s\n",label,op1,hex);
        }
        else{
            printf("%s\tmov\teax, [%s + 4 * %s]\t\t%s\n",label,op1,offsetOp1,hex);
            fprintf(fp,"%s\tmov\teax, [%s + 4 * %s]\t\t%s\n",label,op1,offsetOp1,hex);
        }
        contPosicao += 7;
    }
    else if (strcmp(operacao, "STORE") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        i = consultaRotulo(op1);
        /*printf("%d", rotulos[i].valor);*/
        sprintf(hexaux,"%.2x", rotulos[i].valor);
        /*printf("%d", rotulos[i].valor);*/
        strcpy(hex,"89 04 25 ");
        strcat(hex, hexaux);
        strcat(hex," 00 00 00 ");

        if (offsetOp1[0] == '\0'){
            printf("%s\tmov\tdword [%s], eax\t\t%s\n",label,op1,hex);
            fprintf(fp,"%s\tmov\tdword [%s], eax\t\t%s\n",label,op1,hex);
        }
        else{
            printf("%s\tmov\tdword [%s + 4 *%s], eax\t\t%s\n",label,op1,offsetOp1,hex);
            fprintf(fp,"%s\tmov\tdword [%s + 4 *%s], eax\t\t%s\n",label,op1,offsetOp1,hex);
        }
        contPosicao += 7;
    }
    else if (strcmp(operacao, "INPUT") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        /*le_int = 1;*/


        printf("%s\tcall\tLerInteiro\t\te8 00 \n",label);
        fprintf(fp,"%s\tcall\tLerInteiro\t\te8 00 \n",label);
        printf("\tmov\tebx, [buffer]\t\t8b 1c 25 00 00 00 00 \n");
        fprintf(fp,"\tmov\tebx, [buffer]\t\t8b 1c 25 00 00 00 00 \n");

        i = consultaRotulo(op1);
        /*printf("%d", rotulos[i].valor);*/
        sprintf(hexaux,"%.2x", rotulos[i].valor);
        /*printf("%d", rotulos[i].valor);*/
        strcpy(hex,"89 1c 25 ");
        strcat(hex, hexaux);
        strcat(hex," 00 00 00 ");

        if(offsetOp1[0] == '\0'){
            printf("\tmov\tdword[%s], ebx\t\t%s \n",op1, hex);
            fprintf(fp,"\tmov\tdword[%s], ebx\t\t%s \n",op1, hex);
        }
        else{
            printf("\tmov\tdword[%s + 4 * %s], ebx\t\t%s \n",op1,offsetOp1, hex);
            fprintf(fp,"\tmov\tdword[%s + 4 * %s], ebx\t\t%s \n",op1,offsetOp1, hex);
        }

        contPosicao += 16;
    }
    else if (strcmp(operacao, "OUTPUT") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        /*escreve_int = 1;*/
        i = consultaRotulo(op1);
        /*printf("%d", rotulos[i].valor);*/
        sprintf(hexaux,"%.2x", rotulos[i].valor);
        /*printf("%d", rotulos[i].valor);*/
        strcpy(hex,"8b 1c 25 ");
        strcat(hex, hexaux);
        strcat(hex," 00 00 00 ");

        if(offsetOp1[0] == '\0'){
            printf("%s\tmov\tebx, [%s]\t\t%s \n",label,op1, hex);
            fprintf(fp,"%s\tmov\tebx, [%s]\t\t%s \n",label,op1, hex);
        }
        else{
            printf("%s\tmov\tebx, [%s + 4 *%s]\t\t%s \n",label,op1,offsetOp1, hex);
            fprintf(fp,"%s\tmov\tebx, [%s + 4 *%s]\t\t%s \n",label,op1,offsetOp1, hex);
        }

        printf("\tmov\tdword[buffer], ebx\t\t89 1c 25 00 00 00 00 \n");
        fprintf(fp,"\tmov\tdword[buffer], ebx\t\t89 1c 25 00 00 00 00 \n");
        printf("\tcall\tEscreverInteiro\t\te8 00 \n");
        fprintf(fp,"\tcall\tEscreverInteiro\t\te8 00 \n");
        contPosicao += 16;
    }
    else if (strcmp(operacao, "STOP") == 0){
        if(label[0] != '\0')
            strcat(label,":");
        printf("%s\tmov\teax, 1\t\tb8 01 00 00 00 \n",label);
        printf("\tmov\tebx, 0\t\tbb 00 00 00 00 \n");
        printf("\tint\t80h\t\tcd 80\n");
        fprintf(fp,"%s\tmov\teax, 1\t\tb8 01 00 00 00 \n",label);
        fprintf(fp,"\tmov\tebx, 0\t\tbb 00 00 00 00 \n");
        fprintf(fp,"\tint\t80h\t\tcd 80\n");
        contPosicao += 12;

        /*if (le_int == 1){*/
            printf ("\t\nLerInteiro:\n");
            /*printf ("\tpush ebp\n");
            printf ("\tmov ebp, esp\n");
            printf ("\tpush eax\n");
            printf ("\tpush edx\n");*/
            printf ("\tmov eax, 3\t\tb8 03 00 00 00\n");
            printf ("\tmov ebx, 0\t\tbb 00 00 00 00\n");
            printf ("\tmov ecx, buffer\t\tb9 00 00 00 00\n");
            printf ("\tmov edx, 1\t\tba 01 00 00 00\n");
            printf ("\tint 80h\t\tcd 80\n");
            printf ("\tsub dword [buffer], 0x30\t\t83 2c 25 00 00 00 00 30\n");
            /*printf ("\tpop edx\n");
            printf ("\tpop eax\n");
            printf ("\tpop ebp\n");*/
            printf ("\tret\t\tc3\n");
            fprintf (fp, "\t\nLerInteiro:\n");
            /*fprintf (fp, "\tpush ebp\n");
            fprintf (fp, "\tmov ebp, esp\n");
            fprintf (fp, "\tpush eax\n");
            fprintf (fp, "\tpush edx\n");*/
            fprintf (fp, "\tmov\teax, 3\t\tb8 03 00 00 00\n");
            fprintf (fp, "\tmov\tebx, 0\t\tbb 00 00 00 00\n");
            fprintf (fp, "\tmov\tecx, buffer\t\tb9 00 00 00 00\n");
            fprintf (fp, "\tmov\tedx, 1\t\tba 01 00 00 00\n");
            fprintf (fp, "\tint\t80h\t\tcd 80\n");
            fprintf (fp, "\tsub\tdword [buffer], 0x30\t\t83 2c 25 00 00 00 00 30\n");
            /*fprintf (fp, "\tpop edx\n");
            fprintf (fp, "\tpop eax\n");
            fprintf (fp, "\tpop ebp\n");*/
            fprintf (fp, "\tret\t\tc3\n");
            contPosicao += 31;
        /*}
        if (escreve_int == 1){*/
            printf ("\t\nEscreverInteiro:\n");
            /*printf ("\tpush ebp\n");
            printf ("\tmov ebp, esp\n");
            printf ("\tpush eax\n");
            printf ("\tpush edx\n");*/
            printf ("\tadd\tdword [buffer], 0x30\n");
            printf ("\tmov\teax, 4\n");
            printf ("\tmov\tebx, 1\n");
            printf ("\tmov\tecx, buffer\n");
            printf ("\tmov\tedx, 1\n");
            printf ("\tint\t80h\n");
            /*printf ("\tpop edx\n");
            printf ("\tpop eax\n");
            printf ("\tpop ebp\n");*/
            printf ("\tret\n");
            fprintf (fp, "\t\nEscreverInteiro:\n");
            /*fprintf (fp, "\tpush ebp\n");
            fprintf (fp, "\tmov ebp, esp\n");
            fprintf (fp, "\tpush eax\n");
            fprintf (fp, "\tpush edx\n");*/
            fprintf (fp, "\tadd\tdword [buffer], 0x30\n");
            fprintf (fp, "\tmov\teax, 4\n");
            fprintf (fp, "\tmov\tebx, 1\n");
            fprintf (fp, "\tmov\tecx, buffer\n");
            fprintf (fp, "\tmov\tedx, 1\n");
            fprintf (fp, "\tint\t80h\n");
            /*fprintf (fp, "\tpop edx\n");
            fprintf (fp, "\tpop eax\n");
            fprintf (fp, "\tpop ebp\n");*/
            fprintf (fp, "\tret\n");
            contPosicao += 31;
        /*}*/
    }

    fclose(fp);
    return;
}
void printSectionText(){
	FILE *p1;
	p1=fopen("arquivo.s", "a");
	printf("\n\nsection .text\n");
    printf("global _start\n");
    printf("_start:\n");
	fprintf(p1,"\n\nsection .text\n");
    fprintf(p1,"global _start\n");
    fprintf(p1,"_start:\n");
    fclose(p1);
    return;
}
void printSectionData(){
	FILE *p1;
	p1=fopen("arquivo.s", "a");
	printf("\n\nsection .data\n");
	fprintf(p1,"\n\nsection .data\n");
	fclose(p1);
	return;
}
void sections(char token[], int *secao, bool *data, bool *text){
	token= strtok(NULL, " \t\n");
	if(token == NULL)
		return;
	if (strcmp(token,"TEXT")==0){
		*text=true;
		*secao=TEXT; /*atualiza a secao atual para TEXT*/
	} else if(strcmp(token,"DATA")==0){
		*data=true;
		*secao=DATA; /*atualiza a secao atual PARA DATA*/
	}
	return;
}
void verificaSecao(int secao, int posicao, bool inst){ 	/*secao:indica a secao onde se encontra a instrucao ou diretiva*/
	if(inst){											/*posicao: posicao da diretiva ou instruca no vetor de struct*/
		if(secao!=instrucoes[posicao].secao)			/*inst: identifica se eh uma instrucao ou diretiva*/
			printf("ERROR: Instrucao na secao errada.");

	} else {
		if(secao!=diretivas[posicao].secao)
			printf("ERROR: Diretiva na secao errada.");
	}
	return;
}
void secaoTextDataFaltante(bool data, bool text){
	if(!data)
		printf("ERROR: Falta SECTION DATA. Erro sintatico\n");
	if(!text)
		printf("ERROR: Falta SECTION TEXT. Erro sintatico\n");
	return;
}
void ValidacaoRotulo(char token[]){ /*validacao vai verificar se o primeiro simbolo eh um numero*/
	switch(token[0]){ /*se for um numero vai romper o switch e cair no printf que reporta o erro*/
		case '0': 	break;
		case '1': 	break;
		case '2': 	break;
		case '3': 	break;
		case '4':	break;
		case '5': 	break;
		case '6': 	break;
		case '7': 	break;
		case '8': 	break;
		case '9': 	break;
		default:	return;
	}
	printf("ERROR: Token %s invalido. Rotulo comeca com numero\n", token);
	return;
}
void validaLabel(char token[]){ 							/*verifica se existe mais de um rotulo na mesma linha*/
	short int i;
	while(token[i]){
		if(token[i]==':') 									/*identifica a existencia de um segundo rotulo ou mais ao encontrar ':'*/
			printf("ERROR: Mais de um rotulo na mesma linha.\n");
		i++;
	}
	return;
}
