/*
– declara¸c˜oes ausentes;								.S
– declara¸c˜oes repetidas;								.S
– diretivas ou instru¸c˜oes inv´alidas;					.S
– diretivas ou instru¸c˜oes na se¸c˜ao errada;			.S(testar)
– instru¸c˜oes com a quantidade de operando inv´alida;	.N
– tokens inv´alidos;									.S
– r´otulos repetidos;									.S
– se¸c˜ao (TEXT) faltante;								.S
FALTA: rotulo invalido, 2 rotulos na linha. rotulo repetindo em outra linha */
/*enum dia {domingo, segunda, terca,quarta, quinta, sexta, sabado};*/
/*Tratamento de erros do programa*/
void checkPointerNull();
/*Erros da ementa*/
void declaracaoAusente();     			/*done*/
void declaracaoRepetida();				/*embutido*/
void diretivasInstrucaoInvalida();   	/*done*/
void diretivaInstrucaoSessaoErrada();	/*done*/
void quantidadeOpInvalido(); 			/*nao implementado*/
void tokenInvalido(); 					/*sei nao*/
void rotuloRepetido(); 					/*done*/
void secaoTextDataFaltante(bool data, bool text);   /*done*/
/*
	Para saber se esta na secao errada.
	Ao achar uma secao, grava ela ou ativa um booleano para ela que vai dizer:lendo dentro do segmento tal.
	entao se for encontrado uma instrucao ou comando que nao corresponde ao segmento. imprime erro! de preferencia,
	o booleano pode ser global, ja que por analisar o comando todo sera verificada constantemente passando de parametro para
	todas as funcoes
	Como resolver
	1 inteiro(melhor)
	int:
	0 nao declarado
	1 secao text
	2 secao data 
*/
void verificaSecao(int secao, int posicao, bool inst); /*done*/

/*------------------------------------------------------------------------------
 *	Tratamento de erros do programa
 *------------------------------------------------------------------------------
 */
void checkPointerNull( *pointer){
	if(pointer== NULL){															/*protecao anti segmentation fault*/
		printf("opa, deu ruim! o ponteiro 'fp' eh nulo");
		exit(EXIT_FAILURE);
	}
}
/*------------------------------------------------------------------------------
 *	Erros da ementa
 *------------------------------------------------------------------------------
 */
void declaracaoAusente(){
	printf("ERROR: rotulo nao foi encontrado. Rotulo nao declarado.\n");
	exit(0);
}

void declaracaoRepetida(){
	printf("ERROR: Declaracao repetida.\n");
	exit(0);
}

void diretivasInstrucaoInvalida(){
	printf("ERROR: Diretiva nao encontrado. Diretiva ou instrucao invalida. \n");
	exit(0);
}

void diretivaInstrucaoSessaoErrada(){
	printf("ERROR: Diretiva ou intrucao na sessao errada.\n");
	exit(0);
}

void quantidadeOpInvalido(){ /*ainda nao implementado*/
	printf("ERROR: Quantidade de operandos invalido.\n");
}

void tokenInvalido(){ /*ta certo isso?*/
	printf("ERROR:Token invalido.\n");
	exit(0);
}

void rotuloRepetido(){
	printf("ERROR: simbolo redefinido. \n");
	exit(0);
}

void secaoTextDataFaltante(bool data, bool text){
	if(!data)
		printf("ERROR: Falta SECTION DATA. Erro sintatico\n");
	if(!text)
		printf("ERROR: Falta SECTION TEXT. Erro sintatico\n");
	exit(0);
}

void verificaSecao(int secao, int posicao, bool inst){ 	/*secao:indica a secao onde se encontra a instrucao ou diretiva*/
	if(inst){											/*posicao: posicao da diretiva ou instruca no vetor de struct*/
		if(secao!=instrucoes[posicao].secao)			/*inst: identifica se eh uma instrucao ou diretiva*/
			printf("ERROR: Instrucao na secao errada.");
	} else {
		if(secao!=diretivas[posicao].secao)			
			printf("ERROR: Diretiva na secao errada.");
	}
	return;
}

